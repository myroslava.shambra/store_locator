<?php

declare(strict_types=1);

namespace Academy\Shop\Block;

use Academy\Shop\Model\Shop;
use Academy\Shop\Model\ResourceModel\Shop\Collection as ShopCollection;
use Academy\Shop\Model\ResourceModel\Shop\CollectionFactory as CollectionFactory;
use Academy\Shop\Helper\Data as ShopHelper;
use Magento\Framework\View\Element\Template;

/**
 * Class RandomProduct
 * @package Academy\Book\Block
 */
class Stores extends Template
{
    /**
     * @var Shop
     */
    private $shopModel;
    /**
     * @var CollectionFactory
     */
    private $collectionFactory;
    /**
     * @var ShopHelper
     */
    private $shopHelper;

    /**
     * View constructor.
     * @param Shop $shop
     * @param CollectionFactory $collectionFactory
     * @param ShopHelper $shopHelper
     * @param Template\Context $context
     * @param array $data
     */
    public function __construct(
        Shop $shop,
        CollectionFactory $collectionFactory,
        ShopHelper $shopHelper,
        Template\Context $context,

        array $data = []
    )
    {
        parent::__construct($context, $data);
        $this->shopModel = $shop;
        $this->collectionFactory = $collectionFactory;
        $this->shopHelper = shopHelper;
    }

    /**
     * @return ShopCollection
     */
    public function getShops() : ShopCollection
    {
        /** @var ShopCollection $collection */

        $collection = $this->collectionFactory->create();

        $collection->setOrder("id","asc");

        return $collection;
    }

}
