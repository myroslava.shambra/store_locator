<?php

declare(strict_types=1);

namespace Academy\Shop\Block;

use Academy\Shop\Api\ShopRepositoryInterface;
use Academy\Shop\Api\Data\ShopInterface;
use Academy\Shop\Model\Shop;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\View\Element\Template;

/**
 * Class RandomProduct
 * @package Academy\Shop\Block
 */
class View extends Template
{
    /**
     * @var ShopRepositoryInterface
     */
    protected $shopRepository;

    /**
     * View constructor.
     * @param ShopRepositoryInterface $shopRepository
     * @param Template\Context $context
     * @param array $data
     */
    public function __construct(
        ShopRepositoryInterface $shopRepository,
        Template\Context $context,
        array $data = []
    )
    {
        parent::__construct($context, $data);
        $this->shopRepository = $shopRepository;
    }

    /**
     * @param null $id
     * @return ShopInterface|null
     */
    public function getShop($id = null) :?ShopInterface
    {
        if ($id)
        {
            try {
                /** @var \Academy\Shop\Model\Shop $shop */
                $shop = $this->shopRepository->getById($id);
                return $shop;
            } catch (NoSuchEntityException $ex) {
                return null;
            }
        }
        return null;
    }

}
