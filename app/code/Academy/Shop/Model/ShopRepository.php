<?php

namespace Academy\Shop\Model;

use Academy\Shop\Api\Data;
use Academy\Shop\Api\ShopRepositoryInterface;
use Academy\Shop\Model\ResourceModel\Shop\CollectionFactory as ShopCollectionFactory;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\CouldNotDeleteException;

class ShopRepository implements ShopRepositoryInterface
{
    /**
     * @var ResourceModel\Shop
     */
    private $resource;
    /**
     * @var Data\ShopInterfaceFactory
     */
    private $shopFactory;
    /**
     * @var ShopCollectionFactory
     */
    private $shopCollectionFactory;
    /**
     * @var CollectionProcessorInterface
     */
    private $collectionProcessor;
    /**
     * @var Data\ShopSearchResultsInterfaceFactory
     */
    private $searchResultsFactory;

    /**
     * BookRepository constructor.
     * @param ResourceModel\Shop $resource
     * @param Data\ShopInterfaceFactory $dataBookFactory
     * @param ShopCollectionFactory $shopCollectionFactory
     * @param CollectionProcessorInterface $collectionProcessor
     * @param Data\ShopSearchResultsInterfaceFactory $searchResultsFactory
     */
    public function __construct(
        \Academy\Shop\Model\ResourceModel\Shop $resource,
        \Academy\Shop\Api\Data\ShopInterfaceFactory $dataShopFactory,
        ShopCollectionFactory $shopCollectionFactory,
        CollectionProcessorInterface $collectionProcessor,
        Data\ShopSearchResultsInterfaceFactory $searchResultsFactory
    )
    {

        $this->resource = $resource;
        $this->shopFactory = $dataShopFactory;
        $this->shopCollectionFactory = $shopCollectionFactory;
        $this->collectionProcessor = $collectionProcessor;
        $this->searchResultsFactory = $searchResultsFactory;
    }

    /**
     * @param Data\ShopInterface $shop
     * @return Data\ShopInterface
     * @throws CouldNotSaveException
     */
    public function save(\Academy\Shop\Api\Data\ShopInterface $shop)
    {
        try {
            $this->resource->save($shop);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__($exception->getMessage()));
        }
        return $shop;
    }

    /**
     * @param int $shopId
     * @return mixed
     * @throws NoSuchEntityException
     */
    public function getById($shopId)
    {
        $shop = $this->shopFactory->create();
        $this->resource->load($shop, $shopId);
        if (!$shop->getId()) {
            throw new NoSuchEntityException(__('The shop with the "%1" ID doesn\'t exist.', $shopId));
        }
        return $shop;
    }

    /**
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return Data\BlockSearchResultsInterface
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria)
    {
        /** @var \Magento\Cms\Model\ResourceModel\Block\Collection $collection */
        $collection = $this->shopCollectionFactory->create();

        $this->collectionProcessor->process($searchCriteria, $collection);

        /** @var Data\BlockSearchResultsInterface $searchResults */
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($searchCriteria);
        $searchResults->setItems($collection->getItems());
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * @param Data\ShopInterface $shop
     * @return bool
     * @throws CouldNotDeleteException
     */
    public function delete(\Academy\Shop\Api\Data\ShopInterface $shop)
    {
        try {
            $this->resource->delete($shop);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__($exception->getMessage()));
        }
        return true;
    }

    /**
     * @param int $shopId
     * @return bool
     */
    public function deleteById($shopId)
    {
        return $this->delete($this->getById($shopId));
    }
    public function getByUrlKey($urlKey)
    {
        $shop = $this->shopFactory->create();
        $this->resource->load($shop, $urlKey, ShopInterface::URL_KEY);
        return $shop;
    }
}
