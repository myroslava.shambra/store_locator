<?php

namespace Academy\Shop\Model;

use Academy\Shop\Api\Data\ShopInterface;
use Academy\Shop\Model\Shop\FileInfo;
use Magento\Backend\App\Action\Context;

use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Model;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Class Book
 * @package Academy\Book\Model
 */
class Shop extends AbstractModel implements ShopInterface
{
    protected $_eventPrefix = 'academy_shop';
    protected $_eventObject = 'shop';

    private $resource;

    private $context;
    private $registry;
    private $resourceCollection;
    /**
     * @var StoreManagerInterface
     */
    protected $_storeManager;

    public function _construct()
    {
        $this->_init(\Academy\Shop\Model\ResourceModel\Shop::class);
    }

    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        StoreManagerInterface $storeManager,
        array $data = [])
    {
        parent::__construct(
            $context,
            $registry,
            $resource,
            $resourceCollection,
            $data);

        $this->resource = $resource;
        $this->context = $context;
        $this->registry = $registry;
        $this->resourceCollection = $resourceCollection;
        $this->_storeManager = $storeManager;
    }

    /**
     * @return int|null
     */
    public function getId()
    {
        return parent::getData(self::ID);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return parent::getData(self::NAME);
    }

    /**
     * @return string|null
     */
    public function getDescription()
    {
        return parent::getData(self::DESCRIPTION);
    }

    /**
     * @return string|null
     */
    public function getImage()
    {
        return parent::getData(self::IMAGE);
    }

    /**
     * @return string|null
     */
    public function getCountry()
    {
        return parent::getData(self::COUNTRY);
    }
    /**
     * @return string|null
     */
    public function getCity()
    {
        return parent::getData(self::CITY);
    }
    /**
     * @return string|null
     */
    public function getAddress()
    {
        return parent::getData(self::ADDRESS);
    }

    /**
     * @return string|null
     */
    public function getUrlKey()
    {
        return parent::getData(self::URL_KEY);
    }

    /**
     * @return string|null
     */
    public function getWorkSchedule()
    {
        return parent::getData(self::WORK_SCHEDULE);
    }

    /**
     * @return string|null
     */
    public function getLongitude()
    {
        return parent::getData(self::LONGITUDE);
    }

    /**
     * @return string|null
     */
    public function getLatitude()
    {
        return parent::getData(self::LATITUDE);
    }

    /**
     * @return string|null
     */
    public function getCreationTime()
    {
        return parent::getData(self::CREATION_TIME);
    }

    /**
     * @return string|null
     */
    public function getUpdateTime()
    {
        return parent::getData(self::UPDATE_TIME);
    }

    /**
     * @param int $id
     * @return \Academy\Shop\Api\Data\ShopInterface
     */
    public function setId($id)
    {
        return $this->setData(self::ID, $id);
    }

    /**
     * @param string $name
     * @return \Academy\Shop\Api\Data\ShopInterface
     */
    public function setName(string $name)
    {
        return $this->setData(self::NAME, $name);
    }

    /**
     * @param string $description
     * @return \Academy\Shop\Api\Data\ShopInterface
     */
    public function setDescription(string $description)
    {
        return $this->setData(self::DESCRIPTION, $description);
    }

    /**
     * @param string $image
     * @return \Academy\Shop\Api\Data\ShopInterface
     */
    public function setImage($image)
    {
        return $this->setData(self::IMAGE, $image);
    }

    /**
     * @param string $country
     * @return \Academy\Shop\Api\Data\ShopInterface
     */
    public function setCountry(string $country)
    {
        return $this->setData(self::COUNTRY, $country);
    }
    /**
     * @param string $city
     * @return \Academy\Shop\Api\Data\ShopInterface
     */
    public function setCity(string $city)
    {
        return $this->setData(self::CITY, $city);
    }

    /**
     * @param string $address
     * @return \Academy\Shop\Api\Data\ShopInterface
     */
    public function setAddress(string $address)
    {
        return $this->setData(self::ADDRESS, $address);
    }

    /**
     * @param string $url_key
     * @return \Academy\Shop\Api\Data\ShopInterface
     */
    public function setUrlKey(string $url_key)
    {
        return $this->setData(self::URL_KEY, $url_key);
    }

    /**
     * @param string $work_schedule
     * @return \Academy\Shop\Api\Data\ShopInterface
     */
    public function setWorkSchedule(string $work_schedule)
    {
        return $this->setData(self::WORK_SCHEDULE, $work_schedule);
    }

    /**
     * @param string $longitude
     * @return \Academy\Shop\Api\Data\ShopInterface
     */
    public function setLongitude(string $longitude)
    {
        return $this->setData(self::LONGITUDE, $longitude);
    }

    /**
     * @param string $latitude
     * @return \Academy\Shop\Api\Data\ShopInterface
     */
    public function setLatitude(string $latitude)
    {
        return $this->setData(self::LATITUDE, $latitude);
    }

    /**
     * @param string $time
     *  @return \Academy\Shop\Api\Data\ShopInterface
     */
    public function setCreationTime($time)
    {
        return $this->setData(self::CREATION_TIME, $time);
    }

    /**
     * @param string $time
     *  @return \Academy\Shop\Api\Data\ShopInterface
     */
    public function setUpdateTime($time)
    {
        return $this->setData(self::UPDATE_TIME, $time);
    }

    /**
     * Retrieve the Image URL
     *
     * @param string $imageName
     * @return bool|string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getImageUrl($imageName = null)
    {
        $url = '';
        $image = $imageName;
        if (!$image) {
            $image = $this->getData('image');
        }
        if ($image) {
            if (is_string($image)) {
                $url = $this->_storeManager->getStore()->getBaseUrl(
                    \Magento\Framework\UrlInterface::URL_TYPE_MEDIA
                ) . FileInfo::ENTITY_MEDIA_PATH . '/' . $image;
            } else {
                throw new \Magento\Framework\Exception\LocalizedException(
                    __('Something went wrong while getting the image url.')
                );
            }
        }
        return $url;
    }

}
