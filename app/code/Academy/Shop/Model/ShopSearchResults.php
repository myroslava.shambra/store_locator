<?php

declare(strict_types=1);

namespace Academy\Shop\Model;

use Academy\Shop\Api\Data\ShopSearchResultsInterface;
use Magento\Framework\Api\SearchResults;


class ShopSearchResults extends SearchResults implements ShopSearchResultsInterface
{
}
