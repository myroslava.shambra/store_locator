<?php

namespace Academy\Shop\Model\Import;

use Academy\Shop\Model\Import\CustomImport\RowValidatorInterface as ValidatorInterface;
use Magento\ImportExport\Model\Import\ErrorProcessing\ProcessingErrorAggregatorInterface;
use Magento\Framework\App\ResourceConnection;

class Courses extends \Magento\ImportExport\Model\Import\Entity\AbstractEntity
{
    const ID = 'id';
    const NAME = 'name';
    const COUNTRY = 'country';
    const CITY = 'city';
    const ZIP = 'zip';
    const ADDRESS = 'address';
    const POSITION = 'position';
    const STATE = 'state';
    const DESC = 'description';
    const PHONE = 'phone';
    const CATEGORY = 'category';
    const ACTIONS_SERIALIZED = 'actions_serialized';
    const IMAGE = 'image';
    const STORES = 'stores';
    const SCHEDULE = 'schedule';
    const MARKER_IMG = 'marker_img';
    const SHOW_SCHEDULE = 'show_schedule';
    const URL_KEY = 'url_key';
    const META_TITLE = 'meta_title';
    const META_DESCRIPTION = 'meta_description';
    const META_ROBOTS = 'meta_robots';
    const SHORT_DESCRIPTION = 'short_description';
    const CANONICAL_URL = 'canonical_url';
    const TABLE_Entity = 'store_locator';

    /** * Validation failure message template definitions *
     * @var array
     */
    protected $_messageTemplates = [ValidatorInterface::ERROR_INVALID_TITLE => 'Name is empty',];

    protected $_permanentAttributes = [self::ID];
    /**
     * If we should check column names
     *
     * @var bool
     */
    protected $needColumnCheck = true;
    protected $groupFactory;
    /**
     * Valid column names
     *
     * @array
     */
    protected $validColumnNames = [
        self::ID,
        self::NAME,
        self::COUNTRY,
        self::CITY,
        self::ZIP,
        self::ADDRESS,
        self::POSITION,
        self::STATE,
        self::DESC,
        self::PHONE,
        self::CATEGORY,
        self::ACTIONS_SERIALIZED,
        self::IMAGE,
        self::STORES,
        self::SCHEDULE,
        self::MARKER_IMG,
        self::SHOW_SCHEDULE ,
        self::URL_KEY,
        self::META_TITLE,
        self::META_DESCRIPTION,
        self::META_ROBOTS,
        self::SHORT_DESCRIPTION,
        self::CANONICAL_URL
    ];

    /**
     * Need to log in import history
     *
     * @var bool
     */
    protected $logInHistory = true;

    protected $_validators = [];


    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $_connection;
    protected $_resource;

    /**
     * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
     */
    public function __construct(
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Magento\ImportExport\Helper\Data $importExportData,
        \Magento\ImportExport\Model\ResourceModel\Import\Data $importData,
        \Magento\Eav\Model\Config $config,
        ResourceConnection $resource,
        \Magento\ImportExport\Model\ResourceModel\Helper $resourceHelper,
        \Magento\Framework\Stdlib\StringUtils $string,
        ProcessingErrorAggregatorInterface $errorAggregator,
        \Magento\Customer\Model\GroupFactory $groupFactory)
    {
//        parent::__construct($jsonHelper, $importExportData,
//            $importData, $config, $resource, $resourceHelper,
//            $string, $errorAggregator);
        $this->jsonHelper = $jsonHelper;
        $this->_importExportData = $importExportData;
        $this->_resourceHelper = $resourceHelper;
        $this->_dataSourceModel = $importData;
        $this->_resource = $resource;
        $this->_connection = $resource->getConnection(\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION);
        $this->errorAggregator = $errorAggregator;
        $this->groupFactory = $groupFactory;
    }


    public function getValidColumnNames()
    {
        return $this->validColumnNames;
    }

    /**
     * Entity type code getter.
     *
     * @return string
     */
    public function getEntityTypeCode()
    {
        return 'custom_import';
    }

    /**
     * Row validation.
     *
     * @param array $rowData
     * @param int $rowNum
     * @return bool
     */
    public function validateRow(array $rowData, $rowNum)
    {
        if (isset($this->_validatedRows[$rowNum])) {
            return !$this->getErrorAggregator()->isRowInvalid($rowNum);
        }
        $this->_validatedRows[$rowNum] = true;
        return !$this->getErrorAggregator()->isRowInvalid($rowNum);
    }

    /**
     * Create Advanced message data from raw data.
     *
     * @return bool Result of operation.
     * @throws \Exception
     */
    protected function _importData()
    {
        $this->saveEntity();
        return true;
    }

    /**
     * Save entity
     *
     * @return $this
     */
    public function saveEntity()
    {
        $this->saveAndReplaceEntity();
        return $this;
    }

    /**
     * Replace entity data
     *
     * @return $this
     */
    public function replaceEntity()
    {
        $this->saveAndReplaceEntity();
        return $this;
    }

    /**
     * Deletes entity data from raw data.
     *
     * @return $this
     */
    public function deleteEntity()
    {
        $listTitle = [];
        while ($bunch = $this->_dataSourceModel->getNextBunch()) {
            foreach ($bunch as $rowNum => $rowData) {
                $this->validateRow($rowData, $rowNum);
                if (!$this->getErrorAggregator()->isRowInvalid($rowNum)) {
                    $rowTtile = $rowData[self::ID];
                    $listTitle[] = $rowTtile;
                }
                if ($this->getErrorAggregator()->hasToBeTerminated()) {
                    $this->getErrorAggregator()->addRowToSkip($rowNum);
                }
            }
        }
        if ($listTitle) {
            $this->deleteEntityFinish(array_unique($listTitle), self::TABLE_Entity);
        }
        return $this;
    }

    /**
     * Save and replace entity
     *
     * @return $this
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function saveAndReplaceEntity()
    {
        $behavior = $this->getBehavior();
        $listTitle = [];
        while ($bunch = $this->_dataSourceModel->getNextBunch()) {
            $entityList = [];
            foreach ($bunch as $rowNum => $rowData) {
                if (!$this->validateRow($rowData, $rowNum)) {
                    $this->addRowError(ValidatorInterface::ERROR_INVALID_TITLE, $rowNum);
                    continue;
                }
                if ($this->getErrorAggregator()->hasToBeTerminated()) {
                    $this->getErrorAggregator()->addRowToSkip($rowNum);
                    continue;
                }

                $rowTtile = $rowData[self::ID];
                $listTitle[] = $rowTtile;
                $entityList[$rowTtile][] = [
//                    self::ID => $rowData[self::ID],
                    self::NAME => $rowData[self::NAME],
                    self::DESC => $rowData[self::DESC],
                    self::IMAGE => $rowData[self::IMAGE],
                    self::COUNTRY => $rowData[self::COUNTRY],
                    self::CITY => $rowData[self::CITY],
                    self::ADDRESS => $rowData[self::ADDRESS],
//                    self::WORK_SCHEDULE => $rowData[self::WORK_SCHEDULE],
//                    self::LONGITUDE => $rowData[self::LONGITUDE],
//                    self::LATITUDE => $rowData[self::LATITUDE]
//                    self::COUNTRY => $rowData[self::COUNTRY],
//                    self::CITY => $rowData[self::CITY],
//                    self::ZIP => $rowData[self::ZIP],
//                    self::POSITION => $rowData[self::POSITION],
//                    self::STATE => $rowData[self::STATE],
//                    self::PHONE => $rowData[self::PHONE],
//                    self::CATEGORY => $rowData[self::CATEGORY],
//                    self::ACTIONS_SERIALIZED => $rowData[self::ACTIONS_SERIALIZED],
//                    self::STORES => $rowData[self::STORES],
//                    self::SCHEDULE => $rowData[self::SCHEDULE],
//
//                    self::MARKER_IMG => $rowData[self::MARKER_IMG],
//                    self::SHOW_SCHEDULE => $rowData[self::SHOW_SCHEDULE],
//                    self::URL_KEY => $rowData[self::URL_KEY],
//                    self::META_TITLE => $rowData[self::META_TITLE],
//                    self::META_DESCRIPTION => $rowData[self::META_DESCRIPTION],
//                    self::META_ROBOTS => $rowData[self::META_ROBOTS],
//                    self::SHORT_DESCRIPTION => $rowData[self::SHORT_DESCRIPTION],
//                    self::CANONICAL_URL => $rowData[self::CANONICAL_URL]
                ];
            }
            if (\Magento\ImportExport\Model\Import::BEHAVIOR_REPLACE == $behavior) {
                if ($listTitle) {
                    if ($this->deleteEntityFinish(array_unique($listTitle), self::TABLE_Entity)) {
                        $this->saveEntityFinish($entityList, self::TABLE_Entity);
                    }
                }
            } elseif (\Magento\ImportExport\Model\Import::BEHAVIOR_APPEND == $behavior) {
                $this->saveEntityFinish($entityList, self::TABLE_Entity);
            }
        }
        return $this;
    }

    /**
     * Save custom data.
     *
     * @param array $entityData
     * @param string $table
     * @return $this
     */
    protected function saveEntityFinish(array $entityData, $table)
    {
        if ($entityData) {
            $tableName = $this->_connection->getTableName($table);
            $entityIn = [];
            foreach ($entityData as $id => $entityRows) {
                foreach ($entityRows as $row) {
                    $entityIn[] = $row;
                }
            }

            if ($entityIn) {
                $this->_connection->insertOnDuplicate($tableName, $entityIn, [
//                    self::ID,
                    self::NAME,
                    self::DESC,
                    self::IMAGE,
                    self::COUNTRY,
                    self::CITY,
                    self::ADDRESS,
//                    self::WORK_SCHEDULE,
//                    self::LONGITUDE,
//                    self::LATITUDE
                ]);
            }
        }
        return $this;
    }

    /**
     * Delete custom data.
     *
     * @param array $entityData
     * @param string $table
     * @return $this
     */
    protected function deleteEntityFinish(array $ids, $table)
    {
        if ($table && $listTitle) {
            try {
                $this->countItemsDeleted += $this->_connection->delete(
                    $this->_connection->getTableName($table),
                    $this->_connection->quoteInto('id IN (?)', $ids)
                );
                return true;
            } catch (\Exception $e) {
                return false;
            }

        } else {
            return false;
        }
    }
}









//namespace Academy\Shop\Model\Import;
//use Academy\Shop\Model\Import\CustomImport\RowValidatorInterface as ValidatorInterface;
//use Magento\ImportExport\Model\Import\ErrorProcessing\ProcessingErrorAggregatorInterface;
//use Magento\Framework\App\ResourceConnection;
//class Courses extends \Magento\ImportExport\Model\Import\Entity\AbstractEntity {
//    const ID = 'id';
//    const NAME = 'name';
//    const DESC = 'description';
//    const IMAGE = 'image';
//    const WORK_SCHEDULE = 'work_schedule';
//    const ADDRESS = 'address';
//    const LONGITUDE = 'longitude';
//    const LATITUDE = 'latitude';
//    const TABLE_Entity = 'store_locator';
//
//    /** * Validation failure message template definitions *
//     * @var array */
//    protected $_messageTemplates = [ ValidatorInterface::ERROR_INVALID_TITLE => 'Name is empty',];
//
//    protected $_permanentAttributes = [self::ID];
//    /**
//     * If we should check column names
//     *
//     * @var bool
//     */
//    protected $needColumnCheck = true;
//    protected $groupFactory;
//    /**
//     * Valid column names
//     *
//     * @array
//     */
//    protected $validColumnNames = [
//        self::ID,
//        self::NAME,
//        self::DESC,
//        self::IMAGE,
//        self::ADDRESS,
//        self::WORK_SCHEDULE,
//        self::LONGITUDE,
//        self::LATITUDE
//    ];
//
//    /**
//     * Need to log in import history
//     *
//     * @var bool
//     */
//    protected $logInHistory = true;
//
//    protected $_validators = [];
//
//
//    /**
//     * @var \Magento\Framework\Stdlib\DateTime\DateTime
//     */
//    protected $_connection;
//    protected $_resource;
//
//    /**
//     * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
//     */
//    public function __construct(
//        \Magento\Framework\Json\Helper\Data $jsonHelper,
//        \Magento\ImportExport\Helper\Data $importExportData,
//        \Magento\ImportExport\Model\ResourceModel\Import\Data $importData,
//        \Magento\Eav\Model\Config $config,
//        ResourceConnection $resource,
//        \Magento\ImportExport\Model\ResourceModel\Helper $resourceHelper,
//        \Magento\Framework\Stdlib\StringUtils $string,
//        ProcessingErrorAggregatorInterface $errorAggregator,
//        \Magento\Customer\Model\GroupFactory $groupFactory)
//    {
////        parent::__construct($jsonHelper, $importExportData,
////            $importData, $config, $resource, $resourceHelper,
////            $string, $errorAggregator);
//        $this->jsonHelper = $jsonHelper;
//        $this->_importExportData = $importExportData;
//        $this->_resourceHelper = $resourceHelper;
//        $this->_dataSourceModel = $importData;
//        $this->_resource = $resource;
//        $this->_connection = $resource->getConnection(\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION);
//        $this->errorAggregator = $errorAggregator;
//        $this->groupFactory = $groupFactory;
//    }
//
//
//    public function getValidColumnNames()
//    {
//        return $this->validColumnNames;
//    }
//
//    /**
//     * Entity type code getter.
//     *
//     * @return string
//     */
//    public function getEntityTypeCode()
//    {
//        return 'custom_import';
//    }
//
//    /**
//     * Row validation.
//     *
//     * @param array $rowData
//     * @param int $rowNum
//     * @return bool
//     */
//    public function validateRow(array $rowData, $rowNum)
//    {
//        if (isset($this->_validatedRows[$rowNum])) {
//            return !$this->getErrorAggregator()->isRowInvalid($rowNum);
//        }
//        $this->_validatedRows[$rowNum] = true;
//        return !$this->getErrorAggregator()->isRowInvalid($rowNum);
//    }
//
//    /**
//     * Create Advanced message data from raw data.
//     *
//     * @throws \Exception
//     * @return bool Result of operation.
//     */
//    protected function _importData()
//    {
//        $this->saveEntity();
//        return true;
//    }
//    /**
//     * Save entity
//     *
//     * @return $this
//     */
//    public function saveEntity()
//    {
//        $this->saveAndReplaceEntity();
//        return $this;
//    }
//
//    /**
//     * Replace entity data
//     *
//     * @return $this
//     */
//    public function replaceEntity()
//    {
//        $this->saveAndReplaceEntity();
//        return $this;
//    }
//    /**
//     * Deletes entity data from raw data.
//     *
//     * @return $this
//     */
//    public function deleteEntity()
//    {
//        $listTitle = [];
//        while ($bunch = $this->_dataSourceModel->getNextBunch()) {
//            foreach ($bunch as $rowNum => $rowData) {
//                $this->validateRow($rowData, $rowNum);
//                if (!$this->getErrorAggregator()->isRowInvalid($rowNum)) {
//                    $rowTtile = $rowData[self::ID];
//                    $listTitle[] = $rowTtile;
//                }
//                if ($this->getErrorAggregator()->hasToBeTerminated()) {
//                    $this->getErrorAggregator()->addRowToSkip($rowNum);
//                }
//            }
//        }
//        if ($listTitle) {
//            $this->deleteEntityFinish(array_unique($listTitle),self::TABLE_Entity);
//        }
//        return $this;
//    }
//
//    /**
//     * Save and replace entity
//     *
//     * @return $this
//     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
//     * @SuppressWarnings(PHPMD.NPathComplexity)
//     */
//    protected function saveAndReplaceEntity()
//    {
//        $behavior = $this->getBehavior();
//        $listTitle = [];
//        while ($bunch = $this->_dataSourceModel->getNextBunch()) {
//            $entityList = [];
//            foreach ($bunch as $rowNum => $rowData) {
//                if (!$this->validateRow($rowData, $rowNum)) {
//                    $this->addRowError(ValidatorInterface::ERROR_INVALID_TITLE, $rowNum);
//                    continue;
//                }
//                if ($this->getErrorAggregator()->hasToBeTerminated()) {
//                    $this->getErrorAggregator()->addRowToSkip($rowNum);
//                    continue;
//                }
//
//                $rowTtile= $rowData[self::ID];
//                $listTitle[] = $rowTtile;
//                $entityList[$rowTtile][] = [
//                    self::ID => $rowData[self::ID],
//                    self::NAME => $rowData[self::NAME],
//                    self::DESC => $rowData[self::DESC],
//                    self::IMAGE => $rowData[self::IMAGE],
//                    self::ADDRESS => $rowData[self::ADDRESS],
//                    self::WORK_SCHEDULE => $rowData[self::WORK_SCHEDULE],
//                    self::LONGITUDE=> $rowData[self::LONGITUDE],
//                    self::LATITUDE => $rowData[self::LATITUDE]
//                ];
//            }
//            if (\Magento\ImportExport\Model\Import::BEHAVIOR_REPLACE == $behavior) {
//                if ($listTitle) {
//                    if ($this->deleteEntityFinish(array_unique(  $listTitle), self::TABLE_Entity)) {
//                        $this->saveEntityFinish($entityList, self::TABLE_Entity);
//                    }
//                }
//            } elseif (\Magento\ImportExport\Model\Import::BEHAVIOR_APPEND == $behavior) {
//                $this->saveEntityFinish($entityList, self::TABLE_Entity);
//            }
//        }
//        return $this;
//    }
//
//    /**
//     * Save custom data.
//     *
//     * @param array $entityData
//     * @param string $table
//     * @return $this
//     */
//    protected function saveEntityFinish(array $entityData, $table)
//    {
//        if ($entityData) {
//            $tableName = $this->_connection->getTableName($table);
//            $entityIn = [];
//            foreach ($entityData as $id => $entityRows) {
//                foreach ($entityRows as $row) {
//                    $entityIn[] = $row;
//                }
//            }
//
//            if ($entityIn) {
//                $this->_connection->insertOnDuplicate($tableName, $entityIn,[
//                    self::ID,
//                    self::NAME,
//                    self::DESC,
//                    self::IMAGE,
//                    self::ADDRESS,
//                    self::WORK_SCHEDULE,
//                    self::LONGITUDE,
//                    self::LATITUDE
//                ]);
//            }
//        }
//        return $this;
//    }
//
//    /**
//     * Delete custom data.
//     *
//     * @param array $entityData
//     * @param string $table
//     * @return $this
//     */
//    protected function deleteEntityFinish(array $ids, $table)
//    {
//        if ($table && $listTitle) {
//            try {
//                $this->countItemsDeleted += $this->_connection->delete(
//                    $this->_connection->getTableName($table),
//                    $this->_connection->quoteInto('id IN (?)', $ids)
//                );
//                return true;
//            } catch (\Exception $e) {
//                return false;
//            }
//
//        } else {
//            return false;
//        }
//    }
//}
