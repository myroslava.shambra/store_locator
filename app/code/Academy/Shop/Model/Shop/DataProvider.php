<?php

namespace Academy\Shop\Model\Shop;

use Academy\Shop\Model\ResourceModel\Shop\CollectionFactory;
use Academy\Shop\Model\Shop\FileInfo;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Ui\DataProvider\Modifier\PoolInterface;

use Magento\Framework\Filesystem;

/**
 * Class DataProvider
 */
class DataProvider extends \Magento\Ui\DataProvider\ModifierPoolDataProvider
{
    /**
     * @var \Academy\Shop\Model\ResourceModel\Shop\Collection
     */
    protected $collection;

    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;

    /**
     * @var array
     */
    protected $loadedData;

    /**
     * @var Filesystem
     */
    protected $fileInfo;

    /**
     * Constructor
     *
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param CollectionFactory $shopCollectionFactory
     * @param DataPersistorInterface $dataPersistor
     * @param \Academy\Shop\Model\Shop\FileInfo $fileInfo
     * @param array $meta
     * @param array $data
     * @param PoolInterface|null $pool
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $shopCollectionFactory,
        DataPersistorInterface $dataPersistor,
        FileInfo $fileInfo,
        array $meta = [],
        array $data = [],
        PoolInterface $pool = null
    ) {
        $this->collection = $shopCollectionFactory->create();
        $this->dataPersistor = $dataPersistor;
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data, $pool);
        $this->fileInfo = $fileInfo;
    }

    /**
     * Get data
     *
     * @return array
     */
    public function getData()
    {
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }
        $items = $this->collection->getItems();
        /** @var \Academy\Shop\Model\Shop $shop */
        foreach ($items as $shop) {
            $shop = $this->convertValues($shop);
            $this->loadedData[$shop->getId()] = $shop->getData();
        }

        $data = $this->dataPersistor->get('shop');
        if (!empty($data)) {
            $shop = $this->collection->getNewEmptyItem();
            $shop->setData($data);
            $this->loadedData[$shop->getId()] = $shop->getData();
            $this->dataPersistor->clear('shop');
        }
        return $this->loadedData;
    }


    /**
     * Converts image data to acceptable for rendering format
     *
     * @param \Academy\Shop\Model\Shop $shop
     * @return \Academy\Shop\Model\Shop $shop
     */
    private function convertValues($shop)
    {
        $fileName = $shop->getImage();
        $image = [];
        if ($this->fileInfo->isExist($fileName)) {
            $stat = $this->fileInfo->getStat($fileName);
            $mime = $this->fileInfo->getMimeType($fileName);
            $image[0]['name'] = $fileName;
            $image[0]['url'] = $shop->getImageUrl();
            $image[0]['size'] = isset($stat) ? $stat['size'] : 0;
            $image[0]['type'] = $mime;
        }

        $shop->setImage($image);

        return $shop;
    }
}
