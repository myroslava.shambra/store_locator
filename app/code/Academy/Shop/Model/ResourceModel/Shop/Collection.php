<?php

namespace Academy\Shop\Model\ResourceModel\Shop;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * Class Shop
 * @package Academy\Shop\Model\ResourceModel\Collection
 */
class Collection extends AbstractCollection
{
    public function _construct() //phpcs:ignore Magento2.CodeAnalysis.EmptyBlock
    {
        parent::_init(
            \Academy\Shop\Model\Shop::class,
            \Academy\Shop\Model\ResourceModel\Shop::class
        );
    }
}
