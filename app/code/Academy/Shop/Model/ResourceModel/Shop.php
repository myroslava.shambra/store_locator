<?php

namespace Academy\Shop\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

/**
 * Class Shop
 * @package Academy\Shop\Model
 */
class Shop extends AbstractDb
{
    public function _construct()
    {
        parent::_init("store_locator","id");
    }
}
