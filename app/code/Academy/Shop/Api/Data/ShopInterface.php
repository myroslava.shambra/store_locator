<?php

namespace Academy\Shop\Api\Data;

interface ShopInterface
{
    /**#@+
     * Constants for keys of data array. Identical to the name of the getter in snake case
     */
    const ID                = "id";
    const NAME              = "name";
    const DESCRIPTION       = "description";
    const IMAGE             = "image";
    const COUNTRY           = "country";
    const CITY              = "city";
    const ADDRESS           = "address";
    const WORK_SCHEDULE     = "work_schedule";
    const LONGITUDE         = "longitude";
    const LATITUDE          = "latitude";
    const CREATION_TIME     = "creation_time";
    const UPDATE_TIME       = "update_time";
    const URL_KEY           = 'url_key';
    /**#@-*/

    /**
     * @return int|null
     */
    public function getId();

    /**
     * @return string
     */
    public function getName();

    /**
     * @return string|null
     */
    public function getDescription();

    /**
     * @return string|null
     */
    public function getImage();

    /**
     * @return string|null
     */
    public function getCountry();
    /**
     * @return string|null
     */
    public function getCity();
    /**
     * @return string|null
     */
    public function getAddress();

    /**
    * @return string|null
    */
    public function getUrlKey();

    /**
     * @return string|null
     */
    public function getWorkSchedule();

    /**
     * @return string|null
     */
    public function getLongitude();

    /**
     * @return string|null
     */
    public function getLatitude();

    /**
     * @return string|null
     */
    public function getCreationTime();

    /**
     * @return string|null
     */
    public function getUpdateTime();

    /**
     * @param int $id
     * @return \Academy\Shop\Api\Data\ShopInterface
     */
    public function setId($id);

    /**
     * @param string $name
     * @return \Academy\Shop\Api\Data\ShopInterface
     */
    public function setName(string $name);

    /**
     * @param string $description
     * @return \Academy\Shop\Api\Data\ShopInterface
     */
    public function setDescription(string $description);

    /**
     * @param string $image
     * @return \Academy\Shop\Api\Data\ShopInterface
     */
    public function setImage(string $image);

    /**
     * @param string $country
     * @return \Academy\Shop\Api\Data\ShopInterface
     */
    public function setCountry(string $country);

    /**
     * @param string $city
     * @return \Academy\Shop\Api\Data\ShopInterface
     */
    public function setCity(string $city);

    /**
     * @param string $address
     * @return \Academy\Shop\Api\Data\ShopInterface
     */
    public function setAddress(string $address);

    /**
     * @param string $url_key
     * @return \Academy\Shop\Api\Data\ShopInterface
     */
    public function setUrlKey(string $url_key);

    /**
     * @param string $work_schedule
     * @return \Academy\Shop\Api\Data\ShopInterface
     */
    public function setWorkSchedule(string $work_schedule);

    /**
     * @param string $longitude
     * @return \Academy\Shop\Api\Data\ShopInterface
     */
    public function setLongitude(string $longitude);

    /**
     * @param string $latitude
     * @return \Academy\Shop\Api\Data\ShopInterface
     */
    public function setLatitude(string $latitude);

    /**
     * @param string $time
     *  @return \Academy\Shop\Api\Data\ShopInterface
     */
    public function setCreationTime($time);

    /**
     * @param string $time
     *  @return \Academy\Shop\Api\Data\ShopInterface
     */
    public function setUpdateTime($time);
}
