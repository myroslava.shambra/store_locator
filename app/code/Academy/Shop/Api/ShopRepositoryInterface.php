<?php

namespace Academy\Shop\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

/**
 * CMS page CRUD interface.
 * @api
 * @since 100.0.2
 */
interface ShopRepositoryInterface
{
    /**
     * Save Shop.
     *
     * @param \Academy\Shop\Api\Data\ShopInterface $shop
     * @return \Academy\Shop\Api\Data\ShopInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(\Academy\Shop\Api\Data\ShopInterface $shop);

    /**
     * Retrieve page.
     *
     * @param int $shopId
     * @return \Academy\Shop\Api\Data\ShopInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($shopId);

    /**
     * Retrieve books matching the specified criteria.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Academy\Shop\Api\Data\ShopSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);

    /**
     * Delete page.
     *
     * @param \Academy\Shop\Api\Data\ShopInterface $shop
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(\Academy\Shop\Api\Data\ShopInterface $shop);

    /**
     * Delete page by ID.
     *
     * @param int $shopId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($shopId);
}
