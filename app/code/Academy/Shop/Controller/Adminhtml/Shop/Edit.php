<?php

namespace Academy\Shop\Controller\Adminhtml\Shop;

use Academy\Shop\Api\ShopRepositoryInterface;
use Magento\Backend\App\Action;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;

class Edit extends \Magento\Backend\App\Action
{
    /**
     * @var ShopRepositoryInterface
     */
    private $shopRepository;
    /**
     * @var Registry
     */
    private $registry;
    /**
     * @var PageFactory
     */
    private $pageFactory;

    /**
     * Edit constructor.
     * @param Action\Context $context
     * @param Registry $registry
     * @param ShopRepositoryInterface $shopRepository
     * @param PageFactory $pageFactory
     */
    public function __construct(
        Action\Context $context,
        Registry $registry,
        ShopRepositoryInterface $shopRepository,
        PageFactory $pageFactory
    )
    {
        parent::__construct($context);
        $this->shopRepository = $shopRepository;
        $this->registry = $registry;
        $this->pageFactory = $pageFactory;
    }

    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Academy_Shop::shop';

    public function execute()
    {
        $id = $this->getRequest()->getParam('id');

        try {
            $model = $this->shopRepository->getById($id);
        } catch (NoSuchEntityException $ex) {
            $this->messageManager->addErrorMessage(__('This store no longer exists.'));
            /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
            $resultRedirect = $this->resultRedirectFactory->create();
            return $resultRedirect->setPath('*/*/');
        }

        $this->registry->register('cms_shop', $model);

        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->pageFactory->create();

        return $resultPage;
    }
}
