<?php

namespace Academy\Shop\Controller\Adminhtml\Shop;

use Academy\Shop\Api\ShopRepositoryInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Backend\App\Action;

class MassDelete extends \Magento\Backend\App\Action
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Academy_Shop::shop';

    /**
     * @var ShopRepositoryInterface
     */
    private $shopRepository;

    /**
     * MassDelete constructor.
     * @param Action\Context $context
     * @param ShopRepositoryInterface $shopRepository
     */
    public function __construct(
        Action\Context $context,
        ShopRepositoryInterface $shopRepository
    )
    {
        parent::__construct($context);
        $this->shopRepository = $shopRepository;
    }

    public function execute()
    {
        $selectedIds = $this->getRequest()->getParam("selected");
        foreach ($selectedIds as $shop)
        {
            try {
                $this->shopRepository->deleteById($shop);
            } catch (CouldNotDeleteException $ex) {}
        }
        $redirect = $this->resultRedirectFactory->create();
        return $redirect->setPath("*/*/");
    }

}
