<?php

namespace Academy\Shop\Controller\Adminhtml\Shop;


use Academy\Shop\Api\ShopRepositoryInterface;
use Academy\Shop\Model\Shop;
use Academy\Shop\Model\ShopFactory;
use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Academy\Shop\Model\Shop\ImageUploader;

class Save extends \Magento\Backend\App\Action
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Academy_Shop::shop';

    /**
     * @var PageFactory
     */
    private $pageFactory;
    /**
     * @var ShopRepositoryInterface
     */
    private $shopRepository;
    /**
     * @var ShopFactory
     */
    private $shopFactory;
    /**
     * @var ImageUploader
     */
    private $imageUploader;

    /**
     * Index constructor.
     * @param Context $context
     * @param PageFactory $pageFactory
     * @param ShopRepositoryInterface $shopRepository
     * @param ShopFactory $shopFactory
     * @param ImageUploader $imageUploader
     */
    public function __construct(
        Context $context,
        PageFactory $pageFactory,
        ShopRepositoryInterface $shopRepository,
        ShopFactory $shopFactory,
        ImageUploader $imageUploader
    )
    {
        parent::__construct($context);
        $this->pageFactory = $pageFactory;
        $this->shopRepository = $shopRepository;
        $this->shopFactory = $shopFactory;
        $this->imageUploader = $imageUploader;
        $this->context = $context;
    }

    public function execute()
    {
        $redirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPostValue();

//        print_r($data);
//        exit();
        if (empty($data['id'])) {
            $data['id'] = null;
        }

        $imageName = '';
        if (!empty($data['image'][0]['name'])) {
            $imageName = $data['image'][0]['name'];
            $data['image']=$imageName;
        }

        /** @var Shop $shop */
        $shop = $this->shopFactory->create();

        $shop->setData($data);

        $this->shopRepository->save($shop);
        if ($data['image']) {
            $this->imageUploader->moveFileFromTmp($data['image']);
        }

        return $redirect->setPath("*/*/");
    }
}
