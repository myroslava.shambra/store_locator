<?php

namespace Academy\Shop\Helper;

use Magento\Framework\App\Config\ScopeConfigInterface;

class Data
{
    const STORELOCATOR_ENABLED = "store_locator/shop/enabled";
    const STORELOCATOR_API_KEY = "store_locator/shop/api_key";

    const XML_PATH_IS_ENABLED = "store_locator/shop/enabled";

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * Data constructor.
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(ScopeConfigInterface $scopeConfig)
    {
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * @return mixed
     */
    public function getStorelocatorEnabled()
    {
        return $this->scopeConfig->getValue(self::STORELOCATOR_ENABLED);
    }

    /**
     * @return mixed
     */
    public function getApiKey()
    {
        return $this->scopeConfig->getValue(self::STORELOCATOR_API_KEY);
    }


    public function isEnabled()
    {
        return $this->scopeConfig->getValue(self::XML_PATH_IS_ENABLED);
    }



}
