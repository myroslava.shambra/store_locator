<?php
namespace Academy\Shop\Observer;

use Academy\Shop\Helper\Data;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\UrlInterface;

class SettingsModule implements ObserverInterface
{
    /**
     * @var Data
     */
    private $helper;
    /**
     * @var \Magento\Framework\UrlInterface
     */
    private $url;

    /**
     * SettingsModule constructor.
     * @param Data $helper
     * @param \Magento\Framework\UrlInterface $url
     */
    public function __construct(
        Data $helper,
        UrlInterface $url
    ) {
        $this->helper = $helper;
        $this->url = $url;
    }

    /**
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        if ($this->helper->getStorelocatorEnabled() == false) {
            $norouteUrl = $this->url->getUrl('no-route');
            $observer->getControllerAction()->getResponse()->setRedirect($norouteUrl);
        }
    }
}
