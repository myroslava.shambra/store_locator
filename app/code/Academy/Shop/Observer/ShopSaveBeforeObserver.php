<?php

namespace Academy\Shop\Observer;

use Academy\Shop\Api\Data\ShopInterface;
use Academy\Shop\Helper\Data;
use Academy\Shop\Helper\GoogleApi;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Message\ManagerInterface;


class ShopSaveBeforeObserver implements \Magento\Framework\Event\ObserverInterface
{
    /**
    * @var GoogleApi
     */
    private $googleApi;
    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    private $messageManager;
    /**
     * @var Data
     */
    private $data;

    /**
     * StorelocatorBeforeSave constructor.
     * @param Data $data
     * @param GoogleApi $googleApi
     * @param \Magento\Framework\Message\ManagerInterface $messageManager
     */
    public function __construct(
        Data $data,
        GoogleApi $googleApi,
        ManagerInterface $messageManager
    ) {
        $this->googleApi = $googleApi;
        $this->messageManager = $messageManager;
        $this->data = $data;
    }

    /**
     * @param Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
//        if (!$this->data->getObserverStatus()) {
//            return;
//        }

        /** @var \Academy\Shop\Model\Shop $store */
        //$store = $observer->getEvent()->getObject();
        $store = $observer->getShop();
        if ((doubleval($store->getLatitude()) == 0) || (doubleval($store->getLongitude()) == 0)) {
            $address = $store->getAddress() . ',' . $store->getCity();
            try {
                $googleapi = $this->googleApi->getGoogleApi($address);
                $store->setLongitude($googleapi['longitude'])->setLatitude($googleapi['latitude']);
            } catch (CouldNotSaveException $exception) {
                $this->messageManager->addNoticeMessage($exception->getMessage());
            }
        }
    }

}
