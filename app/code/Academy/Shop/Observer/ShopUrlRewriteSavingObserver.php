<?php
namespace Academy\Shop\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer;
use Magento\UrlRewrite\Model\ResourceModel\UrlRewriteCollection;
use Magento\UrlRewrite\Model\UrlRewriteFactory;

class ShopUrlRewriteSavingObserver implements ObserverInterface
{
    const TARGET_PATH = 'shop/stores/view/id/';
    const REQUEST_PATH = 'stores/';

    /**
     * @var UrlRewriteFactory
     */
    private $urlRewriteFactory;
    /**
     * @var UrlRewriteCollection
     */
    private $urlRewriteCollection;

    /**
     * StorelocatorAfterSave constructor.
     * @param UrlRewriteFactory $urlRewriteFactory
     * @param UrlRewriteCollection $urlRewriteCollection
     */
    public function __construct(
        UrlRewriteFactory $urlRewriteFactory,
        UrlRewriteCollection $urlRewriteCollection
    ) {
        $this->urlRewriteFactory = $urlRewriteFactory;
        $this->urlRewriteCollection = $urlRewriteCollection;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {

        /** @var \Academy\Shop\Model\Shop $shop */
        $shop = $observer->getShop();
        $urlRewriteCollection = $this->urlRewriteCollection->addFieldToFilter(
            'target_path', ['eq' => self::TARGET_PATH . $shop->getUrlKey()]);

        $data = $urlRewriteCollection->getData();
        if (!empty($data)) {
            return;
        }
        $urlRewriteModel = $this->urlRewriteFactory->create();
        $urlRewriteModel->setStoreId(1);
        $urlRewriteModel->setIsSystem(0);
        $urlRewriteModel->setTargetPath(self::TARGET_PATH . $shop->getId());
        $urlRewriteModel->setRequestPath(self::REQUEST_PATH . $shop->getUrlKey());
        $urlRewriteModel->save();
    }
}
