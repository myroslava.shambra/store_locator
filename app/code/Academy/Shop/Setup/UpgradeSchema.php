<?php
namespace Academy\Shop\Setup;
use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
class UpgradeSchema implements  UpgradeSchemaInterface
{
    public function upgrade(SchemaSetupInterface $setup,
                            ModuleContextInterface $context){
        if (version_compare($context->getVersion(), '0.0.2','<')) {
            $setup->getConnection()->addColumn(
                $setup->getTable('store_locator'),
                'url_key',
                [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'length' => 16,
                        'nullable' => false,
                        'default' => '',
                        'comment' => 'url_key',
                    ]
            );
            }
        $setup->endSetup();
    }
}
