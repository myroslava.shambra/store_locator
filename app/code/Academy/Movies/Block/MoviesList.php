<?php

declare(strict_types=1);

namespace Academy\Movies\Block;

use Academy\Movies\Model\Movies;
use Academy\Movies\Model\ResourceModel\Movies\Collection as MoviesCollection;
use Magento\Framework\View\Element\Template;
use Academy\Movies\Model\ResourceModel\Movies\CollectionFactory as CollectionFactory;

/**
 *
 * @package Academy\Movies\Block
 */
class MoviesList extends Template
{
    /**
     * @var Movies
     */
    private $moviesModel;
    /**
     * @var CollectionFactory
     */
    private  $collectionFactory;

    /**
     * View constructor.
     * @param Movies $movies
     * @param CollectionFactory $collectionFactory
     * @param Template\Context $context
     * @param array $data
     */
    public function __construct(
        Movies $movies,
        CollectionFactory $collectionFactory,
        Template\Context $context,
        array $data = []
    )
    {
        parent::__construct($context, $data);
        $this->moviesModel = $movies;
        $this->collectionFactory = $collectionFactory;
    }

    /**
     * @return MoviesCollection
     */
    public function getMovies($id=null) : MoviesCollection
    {
        /** @var   MoviesCollection $collection */
        $collection = $this->collectionFactory->create();

        $collection->addFieldToFilter(
            "year_published",
            ["eq" => 2019]
        );

        $collection->setOrder(
            "year_published",
            "asc"
        );
        return $collection;
    }
}
