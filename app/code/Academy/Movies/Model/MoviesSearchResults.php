<?php

declare(strict_types=1);

namespace Academy\Movies\Model;

use Academy\Movies\Api\Data\MoviesSearchResultsInterface;
use Magento\Framework\Api\SearchResults;


class MoviesSearchResults extends SearchResults implements MoviesSearchResultsInterface
{
}
