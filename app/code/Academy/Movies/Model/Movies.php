<?php

namespace Academy\Movies\Model;

use Magento\Framework\Model\AbstractModel;
use Academy\Movies\Api\Data\MoviesInterface;

/**
 * Class Movies
 * @package Academy\Movies\Model
 */
class Movies extends AbstractModel implements MoviesInterface
{
    public function _construct()
    {
        $this->_init(\Academy\Movies\Model\ResourceModel\Movies::class);
    }

    /**#@-*/
    /**
     * @return int|null
     */
    public function getId()
    {
        return parent::getData(self::MOVIES_ID);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return parent::getData(self::NAME);
    }

    /**
     * @return string|null
     */
    public function getGenre()
    {
        return parent::getData(self::GENRE);
    }

    /**
     * @return string|null
     */
    public function getCountry()
    {
        return parent::getData(self::COUNTRY);
    }

    /**
     * @return string|null
     */
    public function getAuthor()
    {
        return parent::getData(self::AUTHOR);
    }

    /**
     * @return int|null
     */
    public function getYearPublished()
    {
        return parent::getData(self::YEAR_PUBLISHED);
    }

    /**
     * @return string|null
     */
    public function getCreationTime()
    {
        return parent::getData(self::CREATION_TIME);
    }

    /**
     * @return string|null
     */
    public function getUpdateTime()
    {
        return parent::getData(self::UPDATE_TIME);
    }

    /**
     * @param int $id
     * @return \Academy\Movies\Api\Data\MoviesInterface
     */
    public function setId($id)
    {
        return $this->setData(self::MOVIES_ID, $id);
    }

    /**
     * @param string $name
     * @return \Academy\Movies\Api\Data\MoviesInterface
     */
    public function setName($name)
    {
        return $this->setData(self::NAME, $name);
    }

    /**
     * @param string $genre
     * @return \Academy\Movies\Api\Data\MoviesInterface
     */
    public function setGenre($genre)
    {
        return $this->setData(self::GENRE, $genre);
    }

    /**
     * @param string $country
     * @return \Academy\Movies\Api\Data\MoviesInterface
     */
    public function setCountry($country)
    {
        return $this->setData(self::COUNTRY, $country);
    }

    /**
     * @param string $author
     * @return \Academy\Book\Api\Data\BookInterface
     */
    public function setAuthor($author)
    {
        return $this->setData(self::AUTHOR, $author);
    }

    /**
     * @param int $year
     * @return \Academy\Movies\Api\Data\MoviesInterface
     */
    public function setYearPublished($year)
    {
        return $this->setData(self::YEAR_PUBLISHED, $year);
    }

    /**
     * @param string $time
     * @return \Academy\Movies\Api\Data\MoviesInterface
     */
    public function setCreationTime($time)
    {
        return $this->setData(self::CREATION_TIME, $time);
    }

    /**
     * @param string $time
     * @return \Academy\Movies\Api\Data\MoviesInterface
     */
    public function setUpdateTime($time)
    {
        return $this->setData(self::UPDATE_TIME, $time);
    }
}
