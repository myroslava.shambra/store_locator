<?php

namespace Academy\Movies\Model\ResourceModel\Movies;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * Class Movies
 * @package Academy\Movies\Model\ResourceModel\Collection
 */
class Collection extends AbstractCollection
{
    public function _construct() //phpcs:ignore Magento2.CodeAnalysis.EmptyBlock
    {
        parent::_init(
            \Academy\Movies\Model\Movies::class,
            \Academy\Movies\Model\ResourceModel\Movies::class
        );
    }
}
