<?php

namespace Academy\Movies\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

/**
 * CMS page CRUD interface.
 * @api
 * @since 100.0.2
 */
interface MoviesRepositoryInterface
{
    /**
     * Save movies.
     *
     * @param \Academy\Movies\Api\Data\MoviesInterface $movies
     * @return \Academy\Movies\Api\Data\MoviesInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(\Academy\Movies\Api\Data\MoviesInterface $movies);

    /**
     * Retrieve page.
     *
     * @param int $moviesId
     * @return \Academy\Movies\Api\Data\MoviesInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($moviesId);

    /**
     * Retrieve movies matching the specified criteria.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Academy\Movies\Api\Data\MoviesSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);

    /**
     * Delete page.
     *
     * @param \Academy\Movies\Api\Data\MoviesInterface $movies
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(\Academy\Movies\Api\Data\MoviesInterface $movies);

    /**
     * Delete page by ID.
     *
     * @param int $moviesId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($moviesId);
}
