<?php

namespace Academy\Movies\Api\Data;

interface MoviesInterface
{
    /**#@+
     * Constants for keys of data array. Identical to the name of the getter in snake case
     */
    const MOVIES_ID = "movies_id";
    const NAME = "name";
    const GENRE = "genre";
    const COUNTRY = "country";
    const AUTHOR = "author";
    const YEAR_PUBLISHED = "year_published";
    const CREATION_TIME = "creation_time";
    const UPDATE_TIME = "update_time";

    /**#@-*/
    /**
     * @return int|null
     */
    public function getId();

    /**
     * @return string
     */
    public function getName();

    /**
     * @return string|null
     */
    public function getGenre();

    /**
     * @return string|null
     */
    public function getCountry();

    /**
     * @return string|null
     */
    public function getAuthor();

    /**
     * @return int|null
     */
    public function getYearPublished();

    /**
     * @return string|null
     */
    public function getCreationTime();

    /**
     * @return string|null
     */
    public function getUpdateTime();

    /**
     * @param int $id
     * @return \Academy\Movies\Api\Data\MoviesInterface
     */
    public function setId($id);

    /**
     * @param string $name
     * @return \Academy\Movies\Api\Data\MoviesInterface
     */
    public function setName($name);

    /**
     * @param string $genre
     * @return \Academy\Movies\Api\Data\MoviesInterface
     */
    public function setGenre($genre);

    /**
     * @param string $country
     * @return \Academy\Movies\Api\Data\MoviesInterface
     */
    public function setCountry($country);

    /**
     * @param string $author
     * @return \Academy\Book\Api\Data\BookInterface
     */
    public function setAuthor($author);

    /**
     * @param int $year
     * @return \Academy\Movies\Api\Data\MoviesInterface
     */
    public function setYearPublished($year);

    /**
     * @param string $time
     * @return \Academy\Movies\Api\Data\MoviesInterface
     */
    public function setCreationTime($time);

    /**
     * @param string $time
     * @return \Academy\Movies\Api\Data\MoviesInterface
     */
    public function setUpdateTime($time);
}
