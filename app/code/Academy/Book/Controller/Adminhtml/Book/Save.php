<?php

namespace Academy\Shop\Controller\Adminhtml\Shop;


use Academy\Shop\Api\ShopRepositoryInterface;
use Academy\Shop\Model\Shop;
use Academy\Shop\Model\ShopFactory;
use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Save extends \Magento\Backend\App\Action
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Academy_Shop::shop';

    /**
     * @var PageFactory
     */
    private $pageFactory;
    /**
     * @var ShopRepositoryInterface
     */
    private $shopRepository;
    /**
     * @var ShopFactory
     */
    private $shopFactory;

    /**
     * Index constructor.
     * @param Context $context
     * @param PageFactory $pageFactory
     * @param ShopRepositoryInterface $shopRepository
     * @param ShopFactory $shopFactory
     */
    public function __construct(
        Context $context,
        PageFactory $pageFactory,
        ShopRepositoryInterface $shopRepository,
        ShopFactory $shopFactory
    )
    {
        parent::__construct($context);
        $this->pageFactory = $pageFactory;
        $this->shopRepository = $shopRepository;
        $this->shopFactory = $shopFactory;
    }

    public function execute()
    {
        $redirect = $this->resultRedirectFactory->create();

        $data = $this->getRequest()->getPostValue();

        if (empty($data['id'])) {
            $data['id'] = null;
        }

        /** @var Shop $shop */
        $shop = $this->shopFactory->create();

        $shop->setData($data);

        $this->shopRepository->save($shop);

        return $redirect->setPath("*/*/");
    }
}
