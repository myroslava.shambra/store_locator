<?php

namespace Academy\Book\Controller\Adminhtml\Book;

use Academy\Book\Api\BookRepositoryInterface;
use Magento\Backend\App\Action;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;

class Edit extends \Magento\Backend\App\Action
{
    /**
     * @var BookRepositoryInterface
     */
    private $bookRepository;
    /**
     * @var Registry
     */
    private $registry;
    /**
     * @var PageFactory
     */
    private $pageFactory;

    /**
     * Edit constructor.
     * @param Action\Context $context
     * @param Registry $registry
     * @param BookRepositoryInterface $bookRepository
     * @param PageFactory $pageFactory
     */
    public function __construct(
        Action\Context $context,
        Registry $registry,
        BookRepositoryInterface $bookRepository,
        PageFactory $pageFactory
    )
    {
        parent::__construct($context);
        $this->bookRepository = $bookRepository;
        $this->registry = $registry;
        $this->pageFactory = $pageFactory;
    }

    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Academy_Book::book';

    public function execute()
    {
        $id = $this->getRequest()->getParam('book_id');

        try {
            $model = $this->bookRepository->getById($id);
        } catch (NoSuchEntityException $ex) {
            $this->messageManager->addErrorMessage(__('This book no longer exists.'));
            /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
            $resultRedirect = $this->resultRedirectFactory->create();
            return $resultRedirect->setPath('*/*/');
        }

        $this->registry->register('cms_book', $model);

        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->pageFactory->create();

        return $resultPage;
    }
}