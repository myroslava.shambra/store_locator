<?php

declare(strict_types=1);

namespace Academy\Book\Model;

use Academy\Book\Api\Data\BookSearchResultsInterface;
use Magento\Framework\Api\SearchResults;


class BookSearchResults extends SearchResults implements BookSearchResultsInterface
{
}
