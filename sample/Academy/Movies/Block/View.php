<?php

declare(strict_types=1);

namespace Academy\Movies\Block;

use Academy\Movies\Api\MoviesRepositoryInterface;
use Academy\Movies\Api\Data\MoviesInterface;
use Academy\Movies\Model\Movies;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\View\Element\Template;

/**
 * Class RandomProduct
 * @package Academy\Movies\Block
 */
class View extends Template
{
    /**
     * @var MoviesRepositoryInterface
     */
    private $moviesRepository;

    /**
     * View constructor.
     * @param MoviesRepositoryInterface $moviesRepository
     * @param Template\Context $context
     * @param array $data
     */
    public function __construct(
        MoviesRepositoryInterface $moviesRepository,
        Template\Context $context,
        array $data = []
    )
    {
        parent::__construct($context, $data);
        $this->moviesRepository = $moviesRepository;
    }

    /**
     * @param null $id
     * @return MoviesInterface|null
     */
    public function getMovies($id = null) :?MoviesInterface
    {
        if ($id)
        {
            try {
                /** @var \Academy\Movies\Model\Movies $movies */
                $movies = $this->moviesRepository->getById($id);
                return $movies;
            } catch (NoSuchEntityException $ex) {
                return null;
            }
        }
        return null;
    }

}
