<?php

namespace Academy\Movies\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

/**
 * Class Movies
 * @package Academy\Movies\Model
 */
class Movies extends AbstractDb
{
    public function _construct()
    {
        parent::_init("elogic_movies","movies_id");
    }
}
