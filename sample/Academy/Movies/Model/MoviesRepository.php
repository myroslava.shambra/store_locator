<?php

namespace Academy\Movies\Model;

use Academy\Movies\Api\Data;
use Academy\Movies\Api\MoviesRepositoryInterface;
use Academy\Movies\Model\ResourceModel\Movies\CollectionFactory as MoviesCollectionFactory;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;

class MoviesRepository implements MoviesRepositoryInterface
{
    /**
     * @var ResourceModel\Movies
     */
    private $resource;
    /**
     * @var Data\MoviesInterfaceFactory
     */
    private $moviesFactory;
    /**
     * @var MoviesCollectionFactory
     */
    private $moviesCollectionFactory;
    /**
     * @var CollectionProcessorInterface
     */
    private $collectionProcessor;
    /**
     * @var Data\MoviesSearchResultsInterfaceFactory
     */
    private $searchResultsFactory;

    /**
     * BookRepository constructor.
     * @param ResourceModel\Movies $resource
     * @param Data\MoviesInterfaceFactory $dataBookFactory
     * @param MoviesCollectionFactory $moviesCollectionFactory
     * @param CollectionProcessorInterface $collectionProcessor
     * @param Data\MoviesSearchResultsInterfaceFactory $searchResultsFactory
     */
    public function __construct(
        \Academy\Movies\Model\ResourceModel\Movies $resource,
        \Academy\Movies\Api\Data\MoviesInterfaceFactory $dataMoviesFactory,
        MoviesCollectionFactory $moviesCollectionFactory,
        CollectionProcessorInterface $collectionProcessor,
        Data\MoviesSearchResultsInterfaceFactory $searchResultsFactory
    )
    {

        $this->resource = $resource;
        $this->moviesFactory = $dataMoviesFactory;
        $this->moviesCollectionFactory = $moviesCollectionFactory;
        $this->collectionProcessor = $collectionProcessor;
        $this->searchResultsFactory = $searchResultsFactory;
    }

    /**
     * @param Data\MoviesInterface $movies
     * @return Data\MoviesInterface
     */
    public function save(\Academy\Movies\Api\Data\MoviesInterface $movies)
    {
        try {
            $this->resource->save($movies);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__($exception->getMessage()));
        }
        return $movies;
    }

    /**
     * @param int $moviesId
     * @return mixed
     */
    public function getById($moviesId)
    {
        $movies = $this->moviesFactory->create();
        $this->resource->load($movies, $moviesId);
        if (!$movies->getId()) {
            throw new NoSuchEntityException(__('The movies with the "%1" ID doesn\'t exist.', $moviesId));
        }
        return $movies;
    }

    /**
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return Data\BlockSearchResultsInterface
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria)
    {
        /** @var \Magento\Cms\Model\ResourceModel\Block\Collection $collection */
        $collection = $this->moviesCollectionFactory->create();

        $this->collectionProcessor->process($searchCriteria, $collection);

        /** @var Data\BlockSearchResultsInterface $searchResults */
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($searchCriteria);
        $searchResults->setItems($collection->getItems());
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * @param Data\MoviesInterface $movies
     * @return bool
     */
    public function delete(\Academy\Movies\Api\Data\MoviesInterface $movies)
    {
        try {
            $this->resource->delete($movies);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__($exception->getMessage()));
        }
        return true;
    }

    /**
     * @param int $moviesId
     * @return bool
     */
    public function deleteById($moviesId)
    {
        return $this->delete($this->getById($moviesId));
    }
}
